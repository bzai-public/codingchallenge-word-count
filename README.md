# Coding Challenge - wc

## Overview
This is my implementation of the `wc` program as laid out in [codingchallenges.fyi](https://codingchallenges.fyi/challenges/challenge-wc).

The chosen language is *Scala*.

## Motivation
- Tried out [`decline`](https://github.com/bkirwi/decline) library from Typelevel to try and write a fully functional command line app
- Practice functional programming

## Reflections
- Initially wanted to incorporate `cats effect` as well but it would have been too much for this challenge

## To use
1. Build jar by `sbt publishLocal`
2. Run the program using [`ccwc`](./ccwc) script provided
```shell
./ccwc test.txt
```

To understand how to use the command line interface, simply type `./ccwc`. The following help text will appear.
```shell
Usage: Word Count [--bytes] [--lines] [--words] [--chars] <input>

word count

Options and flags:
    --help
        Display this help text.
    --version, -v
        Print the version number and exit.
    --bytes, -c
        Count bytes
    --lines, -l
        Count lines
    --words, -w
        Count words
    --chars, -m
        Count characters
```
