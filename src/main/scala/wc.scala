import cats.effect.*
import cats.implicits.*
import com.monovore.decline.*
import com.monovore.decline.effect.*

import java.io.{File, FileInputStream}
import scala.io.*
import java.nio.file.{Files, Paths}

enum Text:
    case FileText(fstream: FileInputStream)
    case RawText(string: String)
    
    def close(): Unit = {
        this match {
            case f: FileText => f.fstream.close()
            case _ => ()
        }
    }
    
def doAction(text: Text)(f1: (f: Text.FileText) => String)(f2: (s: Text.RawText) => String): String = {
    text match {
        case f: Text.FileText => f1(f)
        case s: Text.RawText => f2(s)
    }
}

object ccwc extends CommandIOApp(
    name = "Word Count",
    header = "word count",
    version = "0.1.0"
) {
    override def main: Opts[IO[ExitCode]] = {
        val input = Opts.argument[String]("input")
        val countBytes = Opts.flag("bytes", short="c", help="Count bytes").orFalse
        val countLines = Opts.flag("lines", short="l", help="Count lines").orFalse
        val countWords = Opts.flag("words", short="w", help="Count words").orFalse
        val countChars = Opts.flag("chars", short="m", help="Count characters").orFalse
        
        (input, countBytes, countLines, countWords, countChars).mapN((i, c, l, w, m) => {
            
            val (cc: Boolean, cl: Boolean, cw: Boolean, cm: Boolean) = (c, l, w, m) match {
                case (false, false, false, false) => (true, true, true, false)
                case _ => (c, l, w, m)
            }
            
            val text: Text = if Files.exists(Paths.get(i)) then {
                Text.FileText(new FileInputStream(new File(i)))
            } else {
                Text.RawText(i)
            }

            val byteCount = if cc then {

                doAction(text)(f => f.fstream.available().toString)(s => s.string.getBytes.length.toString)

            } else ""

            val lineCount = if cl then {

                def f1(f: Text.FileText) = {
                    val bufferedSource = new BufferedSource(f.fstream)

                    bufferedSource.getLines.toList.length.toString
                }

                doAction(text)(f1)(s => s.string.split("\\\\n").length.toString)

            } else ""

            val wordCount = if cw then {

                def f1(f: Text.FileText) = {

                    val bufferedSource = new BufferedSource(new FileInputStream(new File(i)))

                    val lines = bufferedSource.getLines
                    
                    lines.filter(l => l.trim.nonEmpty).map(l => l.trim.split("\\s").length).sum.toString
                    
                }

                def f2(s: Text.RawText) = s.string.split("\\\\n").filter(l => l.trim.nonEmpty).map(l => l.trim.split("\\s").length).sum.toString

                doAction(text)(f1)(f2)
                
            } else ""

            val charCount = if cm then {

                def f1(f: Text.FileText) = Source.fromInputStream(f.fstream).length.toString
                
                doAction(text)(f1)(l => l.string.length.toString)

            } else ""
            
            val allCounts = List(byteCount, lineCount, wordCount, charCount).filter(_.nonEmpty).mkString("\t")
            
            text match {
                case f: Text.FileText => println(allCounts + s"\t$i")
                case s: Text.RawText => println(allCounts)
            }
            
            text.close()

            IO(ExitCode(0))
        })
    }
}