val scala3Version = "3.4.0"

lazy val root = project
    .in(file("."))
    .settings(
        name := "CodingChallenge Word Count",
        version := "0.1.0",
        
        scalaVersion := scala3Version,
        
        libraryDependencies += "com.monovore" %% "decline-effect" % "2.4.1",
        libraryDependencies += "org.typelevel" %% "cats-effect" % "3.5.4"
    )
